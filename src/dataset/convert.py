from os import listdir, mkdir
from os.path import join, exists
from shutil import rmtree as rmdir
from src import FRUITS, INPUT_SHAPE, DATA_FORMAT
from src.dataset import DATA_PATHS
import cv2
import numpy as np


def get_num_frames(filename):
    cap = cv2.VideoCapture(filename)
    return int(cap.get(cv2.CAP_PROP_FRAME_COUNT))


def vidread(filename):
    cap = cv2.VideoCapture(filename)

    # make sure stream is open
    if not cap.isOpened():
        cap.open(0)

    # ret turns false when stream is done
    ret = True
    while ret:
        ret, frame = cap.read()
        if frame is not None:
            yield frame

    # close stream
    cap.release()


def extract_frames(step, verbose=True, overwrite=False):
    # check if output directory exists and overwrite it if allowed
    if exists(DATA_PATHS["extracted"]):
        if overwrite:
            rmdir(DATA_PATHS["extracted"])
        else:
            raise FileExistsError("Cannot overwrite existing dataset")
    mkdir(DATA_PATHS["extracted"])
    for fruit in FRUITS:
        mkdir(join(DATA_PATHS["extracted"], fruit))

    # extract frames for each video in the fruits directory
    if verbose:
        print("extracting frames", end="", flush=True)
    for fruit in FRUITS:
        # reset total frame counter
        i = 0

        # iterate over all frames of each video
        for filename in listdir(join(DATA_PATHS["videos"], fruit)):
            for frame in vidread(join(DATA_PATHS["videos"], fruit, filename)):
                if (i % step) == 0:
                    cv2.imwrite(join(DATA_PATHS["extracted"], fruit, f"{fruit}-{i // step:05}.png"),
                                frame)
                i += 1
    print(" - done")


def prepare_frames(interpolation, verbose=True, overwrite=False):
    # check if output directory exists and overwrite it if allowed
    if exists(DATA_PATHS["prepared"]):
        if overwrite:
            rmdir(DATA_PATHS["prepared"])
        else:
            raise FileExistsError("Cannot overwrite existing dataset")
    mkdir(DATA_PATHS["prepared"])
    for fruit in FRUITS:
        mkdir(join(DATA_PATHS["prepared"], fruit))

    # look up interpolation if its a string
    if isinstance(interpolation, str):
        interpolation = getattr(cv2, "INTER_" + interpolation.upper())

    # extract frames for each video in the fruits directory
    if verbose:
        print("preparing frames", end="", flush=True)
    for fruit in FRUITS:
        # reset total frame counter
        i = 0

        # iterate over all frames
        for filename in listdir(join(DATA_PATHS["extracted"], fruit)):
            # get frame data and size
            frame = cv2.imread(join(DATA_PATHS["extracted"], fruit, filename))
            rows, cols = frame.shape[:2] if DATA_FORMAT == "channels_last" else frame.shape[1:] ## frame is None ##TODO

            # crop and resize frame
            frame = cv2.resize(frame[:, cols // 2 - rows // 2:cols // 2 + rows // 2],
                               dsize=INPUT_SHAPE[:2] if DATA_FORMAT == "channels_last" else INPUT_SHAPE[1:],
                               interpolation=interpolation)

            # write it
            cv2.imwrite(join(DATA_PATHS["prepared"], fruit, f"{fruit}-{i:05}.png"),
                        frame)
            i += 1
    print(" - done")
        

def create_arrays(verbose=True, overwrite=False):
    # check if output directory exists and overwrite it if allowed
    if exists(DATA_PATHS["arrays"]):
        if overwrite:
            rmdir(DATA_PATHS["arrays"])
        else:
            raise FileExistsError("Cannot overwrite existing dataset")
    mkdir(DATA_PATHS["arrays"])

    # extract frames for each video in the fruits directory
    i = 0
    x_train = []
    y_train = []
    x_test = []
    y_test = []
    if verbose:
        print("creating arrays", end="", flush=True)
    for fruit in FRUITS:
        for filename in listdir(join(DATA_PATHS["prepared"], fruit)):
            frame = cv2.imread(join(DATA_PATHS["prepared"], fruit, filename))
            frame = frame.astype("float32") / 255

            # split in ratio 1:11
            if (i % 12) == 0:
                x_test.append(frame)
                y_test.append(fruit)
            else:
                x_train.append(frame)
                y_train.append(fruit)
            i += 1
    print(" - done")

    # save them
    np.save(join(DATA_PATHS["arrays"], r"x_train.npy"), np.asarray(x_train))
    np.save(join(DATA_PATHS["arrays"], r"y_train.npy"), np.asarray(y_train))
    np.save(join(DATA_PATHS["arrays"], r"x_test.npy"), np.asarray(x_test))
    np.save(join(DATA_PATHS["arrays"], r"y_test.npy"), np.asarray(y_test))


def load_data():
    x_train = np.load(join(DATA_PATHS["arrays"], r"x_train.npy"))
    y_train = np.load(join(DATA_PATHS["arrays"], r"y_train.npy"))
    x_test = np.load(join(DATA_PATHS["arrays"], r"x_test.npy"))
    y_test = np.load(join(DATA_PATHS["arrays"], r"y_test.npy"))
    return ((x_train, y_train), (x_test, y_test))
