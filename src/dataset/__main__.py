from argparse import ArgumentParser
from src.dataset import create_arrays, extract_frames, prepare_frames


parser = ArgumentParser()
parser.add_argument("-q", "--quiet",
                    action="store_true",
                    help="surpress progress output")
parser.add_argument("-o", "--overwrite",
                    action="store_true",
                    help="overwrite the dataset if it exists")
parser.add_argument("--step",
                    type=int,
                    default=6,
                    help="the frame steps when extracting the frames")
parser.add_argument("--interpolation",
                    type=str,
                    default="lanczos4",
                    help="the interpolation when preparing the frames")
args = parser.parse_args()


extract_frames(args.step, not args.quiet, args.overwrite)
prepare_frames(args.interpolation, not args.quiet, args.overwrite)
create_arrays(not args.quiet, args.overwrite)
