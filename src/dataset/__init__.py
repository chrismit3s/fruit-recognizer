from os.path import join


DATA_PATHS = {"videos":    join(r"data", r"videos"),
              "extracted": join(r"data", r"extracted"),
              "prepared":  join(r"data", r"prepared"),
              "arrays":    join(r"data", r"arrays")}


from src.dataset.convert import extract_frames, prepare_frames, create_arrays, load_data