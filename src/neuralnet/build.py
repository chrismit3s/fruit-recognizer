from keras.models import Sequential
from keras.layers import Conv2D, MaxPool2D, Flatten, Dense
from src import INPUT_SHAPE, DATA_FORMAT, FRUITS


def build(loss, optimizer, metrics):
    # get base
    model = Sequential()

    # add layers
    # convolutional layers
    model.add(Conv2D(filters= 10, kernel_size=(5, 5), activation="relu", data_format=DATA_FORMAT, input_shape=INPUT_SHAPE))
    model.add(Conv2D(filters= 50, kernel_size=(5, 5), activation="relu", data_format=DATA_FORMAT))
    model.add(Conv2D(filters=100, kernel_size=(3, 3), activation="relu", data_format=DATA_FORMAT))
    # max pooling
    model.add(MaxPool2D(pool_size=(2, 2), data_format=DATA_FORMAT))
    # flatten
    model.add(Flatten())
    # dense layers
    model.add(Dense(units=256, activation="relu"))
    model.add(Dense(units=128, activation="relu"))
    model.add(Dense(units= 64, activation="relu"))
    model.add(Dense(units= 32, activation="relu"))
    model.add(Dense(units= 16, activation="relu"))
    model.add(Dense(units=len(FRUITS), activation="softmax"))

    # compile
    model.compile(loss="categorical_crossentropy",
                  optimizer="adadelta",
                  metrics=["accuracy"])
    return model
