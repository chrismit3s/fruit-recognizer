from keras.models import load_model
from numpy import argmax
from os import listdir
from os.path import join, dirname
from src import INPUT_SHAPE, FRUITS
from src.dataset import load_data
from src.neuralnet import MODELS, build
from time import time, strftime
import keras


# disable compatabilty warning output spam
#keras.backend.tf.compat.v1.logging.set_verbosity("ERROR")


class Categorizer:
    def __init__(self, x=None):
        # get a model
        if x is None:
            self.create()
        else:
            self.load(x)

        # compile the model

    def load(self, x):
        if isinstance(x, int):
            x = join(MODELS, sorted(listdir(MODELS))[x])
        self.model = load_model(x)

    def create(self):
        self.model = build()

    def train(self, batch_size=20, epochs=30, verbose=True):
        # load data
        print("loading dataset", end="", flush=True)
        ((x_train, y_train), valid) = load_data()
        print(" - done")

        # train
        model.fit(x_train, y_train,
                  batch_size=batch_size,
                  epochs=epochs,
                  verbose=1 if verbose else 0,
                  validation_data=valid)

    def predict(self, data):
        if data.shape != INPUT_SHAPE:
            raise ValueError(f"Input data has the wrong shape (expected: {INPUT_SHAPE}; got: {data.shape}")
        return FRUITS[argmax(self.model.predict(data.reshape((1,) + INPUT_SHAPE))[0])]

    def save(self):
        if self.model is not None:
            filename = join(r"models", "model-{strftime('%Y-%m-%dT%H-%M-%S')}.h5")
            self.model.save(filename)
        else:
            raise ValueError("No model set")