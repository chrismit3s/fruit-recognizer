from os.path import join


MODELS = join("models")


from src.neuralnet.model import Categorizer
from src.neuralnet.build import build