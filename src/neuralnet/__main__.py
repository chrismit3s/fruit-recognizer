from src.neuralnet import Categorizer
from src import FRUITS
from os.path import join
import cv2
import numpy as np


# get model
print("loading model", end="", flush=True)
model = Categorizer(-1)
print(" - done")

# testing
print("testing", end="", flush=True)
directory = join(r"data", r"testing")
test_pictures = [r"apple-00.jpg", r"apple-01.jpg", r"none-00.jpg"]
results = []
for picture in test_pictures:
    expected = picture.split("-")[0]
    got = model.predict(cv2.imread(join(directory, picture)))
    results.append((expected, got))
print(" - done")

# print results
print("results:")
print("expected | got")
print("---------+-------")
for exp, got in results:
    print(f"{exp:8} | {got:6}")