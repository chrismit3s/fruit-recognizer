from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.core.window import Window
import kivy


class AndroidApp(App):
    def __init__(self):
        super().__init__()
        self.counter = 0

    def build(self):
        self.layout = BoxLayout(spacing=0, orientation="vertical")
        win_size = Window.size

        # add camera spacer
        self.spacer = Label(text=f"just a spacer, this window is {win_size} " + "A-" * 30,
                            size=(win_size[0],) * 2,
                            size_hint=(None,) * 2,
                            text_size=(win_size[0],) * 2,
                            halign="center",
                            valign="center")
        self.layout.add_widget(self.spacer)

        # add a button
        def callback(inst):
            self.counter += 1
            inst.text = f"ive been pressed {self.counter} times"
            self.label.text = f"the button above me has been pressed {self.counter} times"
        self.btn = Button(text="im a button", on_press=callback)
        self.layout.add_widget(self.btn)

        # add a label
        self.label = Label(text="the button above me has been pressed 0 times")
        self.layout.add_widget(self.label)

        return self.layout
